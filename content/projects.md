---
title: Projects
permalink: /projects/
type: "page"
showTableOfContents: true
---

The projects I'm working on for either personal or professional use.
Both current and orphaned projects with their technical and development
documentation.
