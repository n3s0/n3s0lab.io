---
title: "Some Baseline for Cisco Switches"
date: 2022-12-09T19:57:13-06:00
description: "Notes for basic prep for a Cisco Switch. Will be adding more later."
draft: true
tags: [ "Cisco",  ]
type: post
showTableOfContents: true
---

## Summary 

Will be providing notes for the basic prep of Cisco Switches. This is
generally the bare minimum for setup.

One thing I always like to do - I'll remind you also - is save my
running configuration to my startup configuration. So I don't have to
worry about my switch going down and losing my configuration.

## Cisco Switch Setup

First I console into the switch.

```
Switch>
```

```sh
enable
```

This will show the following prompt.

```sh
Switch#
```

```sh
configure terminal
```

The following configuration prompt will be shown.

```
Switch(config)#
```

```sh
hostname <switch name>
```

```sh
enable secret <password>
```

```sh
line con 0
```

This will show the following line.

```
Switch(config-line)#
```
```
password <password>

login

exit


```sh
line vty 0 15
```

This will have the following console.

```sh
Switch(config-line)#
```

password <password>

login local

exit

Don't send log messages to the console.

```
no logging console
```

Don't send log messages to the console vty terminals.

```
no logging monitor
```

Need to encrypt the passwords in running-config for the console and vty
terminals. This may not be the strongest encryption method. But, it's
better than nothing.

```sh
service password-encryption
```

```
ip ssh version 2
```

```
line vty 0 5
```

```
transport input ssh
ip domain-name <domain name>
crypto key generate rsa
ip ssh time-out 60
```

```
login local
```

Turn off some things that I don't use.

```
no ip http server
no ip http secure-server
```

Move away from VLAN 1.

```
interface vlan 1
no ip address
shutdown
interface vlan 10
ip address <ip address> <subnet>
vlan 10
name managment
exit
```

Configuring default gateway.

```
ip default-gateway <route ip address>
```

Configure a range of ports that I need for access. This is dependent on
the switch. For starters I'll put everything in the new VLAN.

In this scenario I'm working with a 24 port switch.

```
interface range gigabitEthernet 1/0/1 - 24
Switch(config-if-range)#
switchport mode access
switchport access vlan 10
spanning-tree portfast
no shut
```

Generally create a new user that also needs the enable secret to access
the ability to configure the switch. Just as a way to make the device a
little more secure.

```
username <username> privilage 1 password 0 <password>
```
