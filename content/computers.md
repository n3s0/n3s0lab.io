---
title: Computers
permalink: /computers/
type: "page"
showTableOfContents: true
---

Generally my personal computers are geared more toward their use. I'm
not a heavy gamer lately. So, my setup has been geared more toward home
infrastructure and development. This is just a list of my daily drivers
with technical specifications included.

## rubik

This is the laptop I've been carrying around for about three years and
it's served me pretty well. I've written a majority of this website on
it, coded a few applications, experiment with VMs, etc. I generally
carry it arround with me wherever I go. May not be the best laptop on
the market anymore. But, it does the job for me.

The specs for rubik can be found below.

- HP EliteBook 840 G5 (6WU90US#ABA)
    - CPU: Intel i5-8350U @ 1.70GHz
    - Memory: 8 GB - Hynix/Hyundai SODIMM DDR4 2400 MT/s
    - GPU: Intel Corporation UHD Graphics 620 (rev 07) 
    - Network:
      - Wireless: Intel Corporation Dual Band Wireless-AC 8265
      - Ethernet: Intel Corporation Ethernet Connection (4) I219-LM (rev
        21) 
    - Disk: SK hynix BC501 NVMe - 256 GB  
    - OS: Ubuntu Desktop 22.04.1 LTS

One rule of thumb I always have for my daily driver is I encrypt the
full disk for the safety of the data on it.

This has hosted a few operating systems on it with little to no issue. A
list can be found below of what I've installed and tested on it. Some I
will not provide the full version of the OS. I tend to jump from OS to
OS depending on what catches my eye.

- OpenBSD
- FreeBSD
- Windows 10
- Fedora Workstation
- Ubuntu Desktop
- Arch Linux

Considering this laptop is getting a little dated. I'm probably going to
look for a new one soon. It's probably time for something new. At some
point. This laptop will probably become the secondary laptop in case
another fails.

