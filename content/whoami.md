---
title: whoami
permalink: /whoami/
type: "page"
showTableOfContents: true
---

## Me

My name is Timothy Loftus (n3s0) and welcome to my collection of notes. 

## N3s0 || Notes

Think of this site as a collection of notes and past experiences for
everything. I code and dabble in technology a little bit. I like to test
self-hosted applications. Another key interest is infosec. So, the blog
may be more geared toward those things. Other things that can be found
on this site can also fall within the realm of things like plumbing,
electrical, and automotive. If I will need to look it up in the future
again. I'll probably write an article about the things I learn now and
in the future.

## Dotfiles/Environments

Below is the link to my dotfiles. For anyone unfamiliar with this.
Dotfiles are configuration files that reside in your home directory on
Linux/Unix operating systems. Used to configure applications that you
use on a day to day basis.

- [n3s0/dotfiles](https://gitlab.com/n3s0/dotfiles)

## Contact Me

Please contact me with any questions regarding this site or anything 
else you may like to talk about. I love me a good random discussion.

If there are any stragglers that have something they'd like to 
talk about to just get off their chest. They're more than welcome to. 
Where I don't provide advice. I will listen.

Constructive criticism means a lot. If I'm not explaining something 
properly, I will listen and fix as needed.

Best ways to get a hold of me on the Internet are in the "Follow Me" 
within the side bar. Email and Git repositories should be in there.

