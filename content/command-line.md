---
title: Command Line Tools
permalink: /command-line/
type: "page"
showTableOfContents: true
---

Little overview of the command line tools I use on a regular basis. Or
have available on servers of workstation for use. This is catered more
towards Linux/Unix distros.
