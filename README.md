# n3s0.gitlab.io

## Overview

This is the site that contains my personal notes and blog posts.

Below is a link to my other blog.

- [n3s0 || info](https://info.n3s0.tech/)

## Setup

Some information on setup for this site will be provided soon.

Clone the repository.

```sh
git clone https://github.com/n3s0/n3s0.gitlab.io.git
```

